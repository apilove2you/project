import Vue from 'vue'
import VueRouter from 'vue-router'
import { isLoggedIn, clearAuthToken, isAdmin, isSuperUser, removeA, getCookies } from '@/utils/auth'
import { clearProfile, getProfile } from '@/utils/profile'
// import { permissionRoute } from '@/middleware/permissionRoute'

const ToolbarManage = () => import('@/views/ToolbarManage/ToolbarManage.vue')
const PageNotFound = () => import('@/components/pages/PageNotFound/PageNotFound')
const DashboardManage = () => import('@/views/DashboardManage/DashboardManage')
const ServiceManage = () => import('@/views/ServiceManage/ServiceManage')
const DashboardUATManage = () => import('@/views/DashboardUATManage/DashboardUATManage')
const DocumentUATManage = () => import('@/views/DocumentUATManage/DocumentUATManage')
const MyDocumentManage = () => import('@/views/MyDocumentManage/MyDocumentManage')
const PMServiceManage = () => import('@/views/PMServiceManage/PMServiceManage')
const PMServiceDetail = () => import('@/views/PMServiceDetail/PMServiceDetail')
const PMLogDetail = () => import('@/views/PMServiceDetail/PMLogDetail')
const LoginManage = () => import('@/views/LoginManage/LoginManage')
const ExportDashboardPDF = () => import('@/views/ExportDashboardPDF/ExportDashboardPDF')
const LogsAction = () => import('@/views/LogsAction/LogsAction')
const AssignmentManage = () => import('@/views/AssignmentManage/AssignmentManage')
const DocumentFormEditManage = () => import('@/views/DocumentFormEdit/DocumentFormEdit')

const Doggood = () => import('@/views/Doggood/Doggood')
Vue.use(VueRouter)

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

const routes = [

  {
    path: '/',
    name: 'Login',
    component: LoginManage,
    meta: {
      title: 'Login | MNSP Inet Service Plus'
    }
  },
  {
    path: '/toolbar',
    name: 'Toolbar',
    component: ToolbarManage,
    children: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        component: DashboardManage,
        meta: {
          title: 'MNSP Inet Service Plus',
          requiresAuth: true,
          is_user: true
        }
      },
      {
        path: '/service',
        name: 'Service',
        component: ServiceManage,
        meta: {
          title: 'Service',
          requiresAuth: true,
          is_user: true
        }
      },
      {
        path: '/dashboardUAT',
        name: 'DashboardUAT',
        component: DashboardUATManage,
        meta: {
          title: 'DashboardUAT',
          requiresAuth: true,
          is_user: true
        }
      },
      {
        path: '/documentUAT',
        name: 'DocumentUAT',
        component: DocumentUATManage,
        meta: {
          title: 'DocumentUAT',
          requiresAuth: true,
          is_user: true
        }
      },
      {
        path: '/mydocument',
        name: 'mydocument',
        component: MyDocumentManage,
        meta: {
          title: 'MyDocument',
          requiresAuth: true,
          is_user: true
        }
      },
      {
        path: '/Doggood',
        name: 'Doggood',
        component: Doggood,
        meta: {
          title: 'Doggood',
          requiresAuth: true,
          is_user: true
        }
      },
      {
        path: '/pmservice',
        name: 'PMService',
        component: PMServiceManage,
        meta: {
          title: 'PMService',
          requiresAuth: true,
          is_user: true
        }
      },
      {
        path: '/assignment',
        name: 'Assignment',
        component: AssignmentManage,
        meta: {
          title: 'Assignment',
          requiresAuth: true,
          is_user: true
        }
      },
      {
        path: '/DocumentFormEdit',
        name: 'documentformedit',
        component: DocumentFormEditManage,
        meta: {
          title: 'DocumentFormEdit',
          requiresAuth: true,
          is_user: true
        }
      },
      {
        path: '/PMServiceDetail',
        name: 'PMServiceDetail',
        component: PMServiceDetail,
        meta: {
          title: 'PMServiceDetail',
          requiresAuth: true,
          is_user: true
        }
      },
      {
        path: '/PMLogDetail',
        name: 'PMLogDetail',
        component: PMLogDetail,
        meta: {
          title: 'PMLogDetail',
          requiresAuth: true,
          is_user: true
        }
      },
      {
        path: '/ExportDashboardPDF',
        name: 'ExportDashboardPDF',
        component: ExportDashboardPDF,
        meta: {
          title: 'ExportDashboardPDF',
          requiresAuth: true,
          is_user: true
        }
      },
      {
        path: '/LogsAction',
        name: 'LogsAction',
        component: LogsAction,
        meta: {
          title: 'LogsAction',
          requiresAuth: true,
          is_user: true
        }
      }
    ]
  },
  {
    path: '/*',
    component: PageNotFound,
    meta: {
      title: 'Not Found Page'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

const pageAdmin = [

]
const pageUser = [

]

const pagePublic = [
]

var pageSuperUser = [

]
router.beforeEach(async (to, from, next) => {
  var arraygper = ['DocumentUAT', 'DocumentNotification', 'PMService', 'mydocument']
  var getProfiledata = await getProfile()
  if (to.name !== 'Login' && !isLoggedIn()) {
    // console.log(isLoggedIn())
    next({ path: '/' })
  } else if (to.name === 'Login' && isLoggedIn()) {
    next({ path: '/dashboard' })
  } else {
    if (!(getProfiledata && (getProfiledata.pm_id || getProfiledata.user_id === '000')) && arraygper.includes(to.name)) {
      next({ path: '/' })
    } else {
      next()
    }
  }
//   if (to.name === 'Login' && isLoggedIn()) {
//     if (isAdmin()) {
//       next({ path: '/dashboard' })
//     } else if (isSuperUser()) {
//       next({ path: '/dashboard' })
//     } else {
//       next({ path: '/timesheet' })
//     }
//   } else if (to.name !== 'Login' && !isLoggedIn()) {
//     const checkPublic = pagePublic.find(item => item === (to.path.split('/')[to.path.split('/').length - 2]))
//     if (!checkPublic) {
//       next({ path: '/' })
//       clearAuthToken()
//       clearProfile()
//     } else {
//       next()
//     }
//   } else {
//     if (getProfile() && getProfile().staff_id === null) {
//       removeA(pageSuperUser, 'timesheet')
//       pageAdmin.push('timesheet')
//     }
//     const checkPermission = pageAdmin.find(item => item === (to.path.split('/'))[to.path.split('/').length - 1])
//     const checkPermissionUser = pageUser.find(item => item === (to.path.split('/'))[to.path.split('/').length - 1])
//     const checkPermissionSuperUser = pageSuperUser.find(item => item === (to.path.split('/'))[to.path.split('/').length - 1])
//     if (isAdmin()) {
//       if (checkPermissionUser) {
//         if ((to.path.split('/'))[1] === checkPermissionUser) {
//           next({ path: '/*' })
//         } else {
//           next()
//         }
//       }
//       next()
//     } else if (isSuperUser()) {
//       const permissionRoute = getCookies()
//       var to_route = to.path.split('/')[to.path.split('/').length - 1]
//       var compareRoute = permissionRoute.access_rights.find(item => item.method === to_route)
//       if (permissionRoute) {
//         if (to_route === 'staff') {
//           to_route = 'staffdev'
//           if (!(permissionRoute.access_rights.filter(item => item.method === to_route)).length) {
//             to_route = 'staffinfrastructor'
//           }
//         }
//         compareRoute = permissionRoute.access_rights.filter(item => item.method.includes(to_route)).some(data => data.view)
//       }
//       if (checkPermissionSuperUser && compareRoute) {
//         next()
//       } else {
//         if ((to.path.split('/'))[to.path.split('/').length - 1] === checkPermissionSuperUser && (to.path.split('/'))[to.path.split('/').length - 1] === checkPermission && !compareRoute) {
//           next({ path: '/*' })
//         } else {
//           next()
//         }
//       }
//       // } else {
//       //   if ((to.path.split('/'))[1] === checkPermissionSuperUser && (to.path.split('/'))[1] === checkPermission) {
//       //     next({ path: '/*' })
//       //   } else {
//       //     next()
//       //   }
//       // }
//       next()
//     } else {
//       if (checkPermissionUser) {
//         next()
//       } else {
//         if ((to.path.split('/'))[to.path.split('/').length - 1] === checkPermission) {
//           next({ path: '/*' })
//         } else {
//           next()
//         }
//       }
//       next()
//     }
//   }
})

// set title headful page
router.afterEach((to, from) => {
  const DEFAULT_TITLE = `${process.env.VUE_APP_URL_API}${to.path}`
  Vue.nextTick(() => {
    document.title = to.meta.title || DEFAULT_TITLE
  })
})

export default router
