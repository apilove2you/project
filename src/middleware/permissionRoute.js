import { getPermission } from '@/api/permissions'

const permissionRoute = async (to) => {
  var data = await getPermission()
  if (data.result.access_rights.find(item => item.method !== 'AAA - Page')) {
    var result = data.result.access_rights.find(item => item.method === 'Staff')
    if (!result.view) return true
    else return false
  } else {
    return true
  }
}

const permissionAdd = async (page) => {
  var data = await getPermission()
  var result = data.result.access_rights.find(item => item.method === 'Staff')
  if (result.view) return true
  else return false
}

export {
  permissionRoute,
  permissionAdd
}
