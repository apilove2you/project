import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import VueCookies from 'vue-cookies'
import vuetify from './plugins/vuetify'
import Donut from 'vue-css-donut-chart'
import 'vue-css-donut-chart/dist/vcdonut.css'
import Skeleton from 'vue-loading-skeleton'
import VueApexCharts from 'vue-apexcharts'
import VueMask from 'v-mask'
import VueFeather from 'vue-feather'
import ToggleButton from 'vue-js-toggle-button'
Vue.use(ToggleButton)
Vue.use(VueMask, VueCookies)
Vue.component('apexchart', VueApexCharts)
Vue.use(Skeleton)
Vue.use(require('vue-moment'))
Vue.component(VueFeather.name, VueFeather)

Vue.use(Donut)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')