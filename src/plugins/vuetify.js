import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    options: { customProperties: true },
    themes: {
      light: {
        greenstffinfo: '#59A055',
        // default colors
        greeninet: '#36b9cc',
        default: '#757575',
        // transparent: 'transparent',
        grayfooter: '#E5E5E5',
        white_header: '#52afe3',
        bg_submenu_header: '#ecf4eb',
        background_output: '#f7f7f7',
        rating: '#ff9933',

        // text
        text_darkgray: '#4a4a4a',
        text_modal: '#505050',
        text_modal_subtitle: '#666666',
        text_modal_subtitle2: '#777878',
        text_lightgray: '#909191',
        text_nodata: '#b3b7b8',

        // icon
        icon: '#5aa051',

        // table dafault
        table_header_bg: '#5aa051',
        table_header_text: '#ffffff',
        table_row_odd: '#f5f5f5',
        table_row_even: '#ffffff',

        // pagination color
        pagination: '#5aa051',

        // filter
        filter_icon: '#5aa051',
        filter_drawer: '#ecf4eb',

        // button
        add_btn: '#4caf50',
        green_btn: '#5aa051',
        red_btn: '#FF5555',
        gray_btn: '#acb0b1',
        yellow_btn: '#fcc94c',
        yellow_btn2: '#E4A83F',
        orange_btn: '#f29620',
        orange_btn2: '#ff9900',

        // satff leave and summary
        table_header_other_bg: '#6378A7',
        table_header_first_bg: '#DBDEE7',
        table_header_other_text: '#ffffff',
        table_header_first_text: '#000000',
        row_highlight_bg: '#D7DDE9',
        summary_title: '#6378A7',

        // job management
        job_status_pendding: '#F2D600',
        job_status_reserve: '#00C2E0',
        job_status_assigned: '#61BD4F',
        job_status_finish: '#ADB5BD',
        job_status_cancel: '#EB5A46',
        job_status_outsouece: '#82E0AA',
        job_status_internal: '#85C1E9',
        job_status_training: '#FFFF99',
        job_action_icon: '#ec9134',

        // user management
        user_edit_btn: '#40a5f5',
        user_reject_btn: '#ff7f7f',

        // skill management
        skill_delete_icon: '#707070',

        // permission management
        pemission_action_icon: '#ec9134',

        // logs managment
        log_status_on: '#82e0aa',
        log_status_off: '#E57373',

        // dashboard
        card_totalperson: '#ffaf2f',
        card_developer: '#1B9EFF',
        card_itoutsource: '#8c77ee',
        icon_staff_dashboard: '#343241',
        text_titleHead: '#413827',
        text_totalNum: '#413827',
        text_color_dev: '#253541',
        text_color_it: '#343241',
        text_title_on: '#9e9e9e',
        text_count_on: '#9e9e9e',
        text_title_ava: '#5aa056',
        text_count_ava: '#5aa056',
        statuschart_onboard: '#4C5866',
        statuschart_availiable: '#00E39F',
        text_onboard: '#4C5866',
        text_availiable: '#00E39F',
        chart_label: '#020202',
        chart_shadow: '#000000',
        projectbar_color1: '#008FF7',
        projectbar_color2: '#8C77EE',
        projectbar_color3: '#FFAF2F',
        projectbar_color4: '#FF455F',
        projectbar_color5: '#00E39B',

        // profile
        text_profile: '#505050',
        rating_icon: '#ec9134',

        // timesheet user
        timesheet_select_borderbtn: '#BDBDBD',
        timesheet_select_text: '#5F6368',
        timesheet_select_hover_btn: '#ECF4EB',
        timesheet_select_hover_text: '#5AA051',

        // modal add member
        text_add_member: '#777878',
        background_member: '#e0e0e0',

        // modal edit permiss
        text_edit_permiss: '#616161',
        // login
        text_login: '#F0F2F5',
        text_header: '#6d6e71',
        login_btn: '#174a67',
        login_btn_bg: '#109C27',
        form_card_shadow: '#888888',

        // department
        dapartment_gray: '#8f938e',
        dapartment_gray2: '#7d807d',
        dapartment_title: '#eeeeee',
        dapartment_tel: '#1967c0',
        dapartment_header_bg: '#dceff5',

        // others
        avatar_shawdow: '#cccccc',
        calendar_title: '#616161',
        border_gray: '#9B9C9C',
        bg_darkgray: '#314051',
        import_excel_tf: '#26247B',
        img_preview: '#dddddd',
        output_bg: '#f7f7f7',
        name_member: '#e0e0e0',

        // toolbar
        text_toolbar: '#000000'

      }
    }
  }
})
