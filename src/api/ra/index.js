import axios from 'axios'

const getDeptAndCompanyAll = () => {
  return new Promise((resolve, reject) => {
    axios.post('https://inet-ra.inet.co.th/api/vMonk/externalApi/searchAllUser',
      {
        image: false
      }, {
        headers: {
          Authorization: 'Bearer e7960fc54a54049fdffcc0343d8e0d7b047177345a3eb75911e83aa9b6b570d7769ac8fe5242e5937292b1f9846bc19ec4e1f28d08cb8ec3a282218fd3427758'
        }
      }).then(item => {
      // console.log(item.data.result)
      resolve(item.data.result)
    }).catch(err => {
      reject(err)
    })
  })
}
export { getDeptAndCompanyAll }
