import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const getDashboard = (filter, filterdate) => {
  return new Promise((resolve, reject) => {
    const data = filter === undefined ? '' : filter
    const date = filterdate === undefined ? '' : filterdate
    axios.get(process.env.VUE_APP_URL_API + '/dashboards?status=' + data + '&date=' + date, { headers: { Authorization: getAuthToken() } }).then(item => {
      resolve(item)
    }).catch(err => {
      // eslint-disable-next-line prefer-promise-reject-errors
      reject({ ...err })
    })
  })
}

const dashboardCenter = (data, page, size, search) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/dashboardCenter/?page=' + page + '&size=' + size + '&search=' + search, data, { headers: { Authorization: getAuthToken() } })
      // .post(process.env.VUE_APP_URL_API + `/dashboardCenter/?page=${query.page}&size=${query.size}`, data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
        // console.log(item.data, 'showAPI')
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getservicetablestatus = (data) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/service/table?status=' + data)
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

export { getDashboard, dashboardCenter, getservicetablestatus }
