import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const ratingNscoring = (data) => {
  data.business_id = data.business_id[0] === 'ทั้งหมด' ? null : data.business_id
  data.team = data.team[0] === 'ทั้งหมด' ? null : data.team
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/ratingNscore', data)
      .then(item => {
        // console.log('item', item)
        resolve(item.data)
      }).catch((error) => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...error })
      })
  })
}

const getDropdown = (id) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getTeam/' + id + '/dept', { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getDetailDepart = (id) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/latestJob/' + id, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data.data)
      }).catch((err) => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getJobDescription = (data, id) => {
  var sent = { id: id, data: data }
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/jobDescription', sent, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

export { ratingNscoring }
