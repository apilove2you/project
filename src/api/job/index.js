import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const objectNotNull = obj => {
  var newObj = []
  for (const key in obj) {
    if (obj[key]) {
      newObj.push(obj[key])
    }
  }
  return newObj.length ? newObj : ['ทั้งหมด']
}

const getJobs = (page, size, search, status, filter) => {
  if (!search) {
    search = ''
  }
  const fillSearch = {
    status_role: objectNotNull(filter.statusrole) || ['ทั้งหมด'],
    status_site: objectNotNull(filter.statussite) || ['ทั้งหมด'],
    jobs_type: objectNotNull(filter.jobtype) || ['ทั้งหมด']
  }
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/listJobs?page=' + page + '&size=' + size + '&search=' + search + '&status=' + status, fillSearch, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getJobsCount = (search = '', filter) => {
  const fillSearch = {
    status_role: objectNotNull(filter.statusrole) || ['ทั้งหมด'],
    status_site: objectNotNull(filter.statussite) || ['ทั้งหมด'],
    jobs_type: objectNotNull(filter.jobtype) || ['ทั้งหมด']
  }
  if (!search) {
    search = ''
  }
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/countjob?search=' + search, fillSearch, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getTraining = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/trainingCourse', { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getTrainingById = (id) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getCourse/' + id, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const EditJob = (data) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/editJobs', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const addMember = (data) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/createJobs', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getMemberBySkill = (data) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/listStaffJobs', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const changeStatus = (data) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/changeStatus', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const edittransferjobs = (data) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/edittransferjobs', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const searchAllUser = (data, page) => {
//   console.log('data, page', data)
//   console.log('data, page', page)
  return new Promise((resolve, reject) => {
    axios
      .post('https://inet-ra.inet.co.th/api/vMonk/externalApi/searchAllUser', data, { headers: { Authorization: 'Bearer e7960fc54a54049fdffcc0343d8e0d7b047177345a3eb75911e83aa9b6b570d7769ac8fe5242e5937292b1f9846bc19ec4e1f28d08cb8ec3a282218fd3427758' } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

export { getJobs, addMember, getMemberBySkill, EditJob, changeStatus, getJobsCount, getTraining, getTrainingById, searchAllUser, edittransferjobs }
