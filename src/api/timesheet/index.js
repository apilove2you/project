import axios from 'axios'
import { getAuthToken } from '../../utils/auth'
import { getProfile } from '../../utils/profile'

const objectNotNull = (obj) => {
  var newObj = []
  for (const key in obj) {
    if (obj[key]) {
      newObj.push(obj[key])
    }
  }
  return newObj
}

const getProject = (search, type, date, page, size) => {
  return new Promise((resolve, reject) => {
    var profile = getProfile()
    var param = {}
    param.staff_id = profile.staff_id ? profile.staff_id : null
    param.search = search
    var query = '?date=' + date + '&page=' + page + '&size=' + size
    axios
      .post(process.env.VUE_APP_URL_API + '/sheetList/' + type + query, param, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getMember = (search, id, page, size, staffid) => {
  return new Promise((resolve, reject) => {
    var param = {}
    param.job_id = id
    param.search = search
    param.staff_id = staffid
    var query = '?page=' + page + '&size=' + size
    axios
      .post(process.env.VUE_APP_URL_API + '/memberList/' + query, param, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getCalendar = (id) => {
  return new Promise((resolve, reject) => {
    var param = {}
    param.staffjob_id = id
    axios
      .post(process.env.VUE_APP_URL_API + '/calendarView/', param, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const createCalendar = (param) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/sheetLog/add', param, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const editCalendar = (param) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/sheetLog/edit/', param, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getJobList = (search, page, size, type, active) => {
  return new Promise((resolve, reject) => {
    if (!search) search = ''
    var profile = getProfile()
    var query = '?search=' + search + '&page=' + page + '&size=' + size + '&status_role=' + type + '&active=' + active
    axios
      .get(process.env.VUE_APP_URL_API + '/myJob/' + profile.staff_id + query, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getAllJobList = (search, page, size, type, active) => {
  return new Promise((resolve, reject) => {
    if (!search) search = ''
    var query = '?search=' + search + '&page=' + page + '&size=' + size + '&status_role=' + type + '&active=' + active
    axios
      .get(process.env.VUE_APP_URL_API + '/jobList' + query, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}
// const getStaffList = (data) => {
//   return new Promise((resolve, reject) => {
//     axios
//       .post(process.env.VUE_APP_URL_API + '/staffList', { headers: { Authorization: getAuthToken() } })
//       .then(item => {
//         resolve(item.data)
//       }).catch(err => {
//         // eslint-disable-next-line prefer-promise-reject-errors
//         reject({ ...err })
//       })
//   })
// }

const getStaffList = (search, site, available, status, team, outsource, statussite, page, size, date) => {
  return new Promise((resolve, reject) => {
    const fillSearch = {
      search: search,
      site: objectNotNull(site),
      available: objectNotNull(available),
      status: objectNotNull(status),
      team: objectNotNull(team),
      outsource: objectNotNull(outsource),
      status_site: objectNotNull(statussite)
    }
    axios
      .post(process.env.VUE_APP_URL_API + '/staffList?page=' + page + '&size=' + size + '&date=' + date, fillSearch, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

export { getProject, getMember, getCalendar, createCalendar, editCalendar, getStaffList, getJobList, getAllJobList }
