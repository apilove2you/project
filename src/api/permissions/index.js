import axios from 'axios'
import { getAuthToken } from '@/utils/auth'
import { getProfile } from '@/utils/profile'

const getPermission = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getPermission?user_id=5ffe9c2059f7dd001782bda1', { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch((err) => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err.response.data })
      })
  })
}

export { getPermission }
