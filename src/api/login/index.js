import axios from 'axios'

const getLogin = (user) => {
  return new Promise((resolve, reject) => {
    axios.post(process.env.VUE_APP_URL_API + '/login',
      {
        username: user.username,
        password: user.password
      }
    )
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        reject(err)
      })
  })
}

export { getLogin }
