import axios from 'axios'
import { getAuthToken } from '../../utils/auth'
import { getProfile } from '@/utils/profile'

const addSoDetail = (dataDetail) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/addSo', dataDetail)
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}
const getStatus = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getStatus')
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const editSoDetail = (soDetail) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/editSO', soDetail)
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const deleteSo = (soData) => {
  var xml = getProfile()
  var keeplog = {
    pm_id: xml.pm_id ? xml.pm_id : 'admin',
    aLog_type: 'delete',
    aLog_dtl: 'delete so detail',
    aLog_to: soData.so_id,
    aLog_loc: 'so_detail'
  }
  soData.keep_log = keeplog
  return new Promise((resolve, reject) => {
    axios
      .put(process.env.VUE_APP_URL_API + '/delSo', soData)
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const activeSoDetail = (soData) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/activeSoDetail', {
        so_activate: soData.so_activate
      })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const inActiveSoDetail = (soData) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/inActiveSoDetail', {
        so_activate: soData.so_activate
      })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}
const addCustomer = (dataCus) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/addCus', dataCus)
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const editCus = (customerData) => {
  // var xml = getProfile()
  // var keeplog = {
  //   pm_id: xml.pm_id ? xml.pm_id : 'admin',
  //   aLog_type: 'edit',
  //   aLog_dtl: 'edit customer',
  //   aLog_to: customerData.cus_no,
  //   aLog_loc: 'customer'
  // }
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/editCus', customerData)
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

// const deleteCus = (customerData) => {
//   return new Promise((resolve, reject) => {
//     axios
//       .delete(process.env.VUE_APP_URL_API + '/deleteCus', {
//         // cus_id: customerData.cus_id,
//         // cus_name_th: customerData.cus_name_th,
//         // cus_name_en: customerData.cus_name_en
//         ...customerData
//       })
//       .then(item => {
//         resolve(item.data)
//       }).catch(err => {
//         // eslint-disable-next-line prefer-promise-reject-errors
//         reject({ ...err })
//       })
//   })
// }

const removeCus = (cusData) => {
  var xml = getProfile()
  var keeplog = {
    pm_id: xml.pm_id ? xml.pm_id : 'admin',
    aLog_type: 'delete',
    aLog_dtl: cusData.state + ' delete customer',
    aLog_to: cusData.ref_id,
    aLog_loc: 'customer'
  }
  return new Promise((resolve, reject) => {
    axios
      .put(process.env.VUE_APP_URL_API + '/request/remove/apv/cus', {
        ref_id: cusData.ref_id,
        state: cusData.state,
        keep_log: keeplog
      })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const delteCus = (cusData) => {
  var xml = getProfile()
  var keeplog = {
    pm_id: xml.pm_id ? xml.pm_id : 'admin',
    aLog_type: 'edit',
    aLog_dtl: 'request remove customer',
    aLog_to: cusData.cus_no,
    aLog_loc: 'customer'
  }
  return new Promise((resolve, reject) => {
    axios
      .put(process.env.VUE_APP_URL_API + '/request/removeCus', {
        pm_id: cusData.pm_id,
        cus_no: cusData.cus_no,
        keep_log: keeplog
      })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const addLog = (dataLog) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/addLog', dataLog, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}
// const getLog = () => {
//   return new Promise((resolve, reject) => {
//     axios
//       .get(process.env.VUE_APP_URL_API + '/getLog')
//       .then(item => {
//         resolve(item.data)
//       }).catch(err => {
//         // eslint-disable-next-line prefer-promise-reject-errors
//         reject({ ...err })
//       })
//   })
// }
const editLog = (editLog, sLogid) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/logs/editLog/' + sLogid, editLog)
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const delLog = (logID) => {
  var xml = getProfile()
  var keeplog = {
    pm_id: xml.pm_id ? xml.pm_id : 'admin',
    aLog_type: 'delete',
    aLog_dtl: 'remove log',
    aLog_to: logID,
    aLog_loc: 'slog'
  }
  return new Promise((resolve, reject) => {
    axios
      .put(process.env.VUE_APP_URL_API + '/logs/delLog/' + logID, { keep_log: keeplog })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getchkCusExist = (data) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/chkCusExist/' + data)
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getApprove = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getApprove')
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getCus = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getCus')
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const addLogformdata = (editLog) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/logs/addLog', editLog)
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const chkDupp = (state, sono, soid = null) => {
    let str = "/chkSOdupp/" + state + "/" + sono
    str = soid ? str + "?soID=" + soid : str
    return new Promise((resolve, reject) => {
        axios
            .get(process.env.VUE_APP_URL_API + str)
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

export { addSoDetail, addLog, editLog, delteCus, delLog, addCustomer, getApprove, editCus, editSoDetail, deleteSo, activeSoDetail, inActiveSoDetail, getStatus, addLogformdata, getCus, removeCus, chkDupp, getchkCusExist }
