import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const objectNotNull = obj => {
  var newObj = []
  for (const key in obj) {
    if (obj[key]) {
      newObj.push(obj[key])
    }
  }
  return newObj.length ? newObj : ['ทั้งหมด']
}

const EditDetail = data => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/editStaffJob', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const EditProfile = data => {
  return new Promise((resolve, reject) => {
    axios
      .put(process.env.VUE_APP_URL_API + '/editProfile', data, {
        headers: { Authorization: getAuthToken() }
      })
      .then(item => {
        resolve(item.data)
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const AddProfile = data => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/newStaff', data, {
        headers: { Authorization: getAuthToken() }
      })
      .then(item => {
        resolve(item.data)
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err.response.data })
      })
  })
}

const showSkill = data => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getSkill', {
        headers: { Authorization: getAuthToken() }
      })
      .then(item => {
        resolve(item.data)
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getSearch = (
  search,
  site,
  available,
  status,
  team,
  outsource,
  statussite,
  skill,
  page,
  size,
  date
) => {
  return new Promise((resolve, reject) => {
    const fillSearch = {
      search: search,
      site: objectNotNull(site),
      available: objectNotNull(available),
      status: objectNotNull(status),
      team: team,
      outsource: objectNotNull(outsource),
      leave: ['ทำงานอยู่'],
      status_site: objectNotNull(statussite),
      skill: objectNotNull(skill)
    }
    axios
      .post(
        process.env.VUE_APP_URL_API +
          '/search/view?page=' +
          page +
          '&size=' +
          size +
          '&date=' +
          date,
        fillSearch,
        { headers: { Authorization: getAuthToken() } }
      )
      .then(item => {
        resolve(item.data)
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const UploadCV = cv => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/imports/pdf/cv', cv, {
        headers: { Authorization: getAuthToken() }
      })
      .then(item => {
        resolve(item.data)
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err.response.data })
      })
  })
}

const DeleteCV = (id, fileid) => {
  return new Promise((resolve, reject) => {
    axios
      .delete(process.env.VUE_APP_URL_API + '/pdf/cv/' + id + '/' + fileid, {
        headers: { Authorization: getAuthToken() }
      })
      .then(item => {
        resolve(item.data)
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err.response.data })
      })
  })
}

const getStaffById = id => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getStaff/' + id, {
        headers: { Authorization: getAuthToken() }
      })
      .then(item => {
        resolve(item.data)
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err.response.data })
      })
  })
}

const addLeaveJob = data => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/addLeaveJob', data, {
        headers: { Authorization: getAuthToken() }
      })
      .then(item => {
        resolve(item.data)
      })
      .catch(err => {
        // console.log(err.response)
        reject(new Error(JSON.stringify({ status: err.response.status, message: err.response.data.message })))
        // eslint-disable-next-line prefer-promise-reject-errors
        // reject({ ...err.response.data })
      })
  })
}

const exportStaff = (
  search,
  site,
  available,
  status,
  team,
  outsource,
  statussite,
  skill,
  page,
  size,
  date
) => {
  return new Promise((resolve, reject) => {
    const fillSearch = {
      search: search,
      site: objectNotNull(site),
      available: objectNotNull(available),
      status: objectNotNull(status),
      team: team,
      outsource: objectNotNull(outsource),
      leave: ['ทำงานอยู่'],
      status_site: objectNotNull(statussite),
      skill: objectNotNull(skill)
    }
    // console.log(fillSearch)
    axios
      .post(
        process.env.VUE_APP_URL_API + '/search/export?date=' + date,
        fillSearch,
        {
          headers: { Authorization: getAuthToken() }
        }
      )
      .then(item => {
        // console.log(item)
        resolve(item.data)
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err.response.data })
      })
  })
}

const getStaffLeave = (data) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/getStaffLeave', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err.response.data })
      })
  })
}

export { getSearch, EditDetail, EditProfile, AddProfile, showSkill, UploadCV, getStaffById, DeleteCV, addLeaveJob, getStaffLeave, exportStaff }
