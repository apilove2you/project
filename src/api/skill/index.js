import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const showSkill = (skill = '', page, size) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getSkill?search=' + skill, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      })
  })
}

const delSkill = (id, skillname) => {
  return new Promise((resolve, reject) => {
    axios
      .delete(process.env.VUE_APP_URL_API + '/dropSkill?id=' + id + '&skill_name=' + skillname, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      })
  })
}

const UploadImg = (data) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/newskill', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch((err) => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err.response.data })
      })
  })
}

export { showSkill, UploadImg, delSkill }
