import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const importExcelStaff = (formData) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/imports/excel/staff', formData, {
        headers: { 'X-Content-Type-Options': 'nosniff', authorization: getAuthToken() }
      })
      .then(item => {
        resolve(item)
      })
  })
}

const importExcelJob = (formData) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/imports/excel/job', formData, {
        headers: { 'X-Content-Type-Options': 'nosniff', authorization: getAuthToken() }
      })
      .then(item => {
        resolve(item)
      })
  })
}

const importExcelMember = (formData) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/imports/excel/member', formData, {
        headers: { 'X-Content-Type-Options': 'nosniff', authorization: getAuthToken() }
      })
      .then(item => {
        resolve(item)
      })
  })
}

export { importExcelStaff, importExcelJob, importExcelMember }
