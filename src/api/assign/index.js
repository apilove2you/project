import axios from 'axios'
import { getProfile } from '@/utils/profile'

const getAssigntable = (data, type = 'noti') => {
    const pm_id = data === 'admin' ? 'pm0' : data.pm_id
    return new Promise((resolve, reject) => {
        axios
            .get(process.env.VUE_APP_URL_API + `/assign/table/${pm_id}/${type}`)
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

const modaltable = () => {
    return new Promise((resolve, reject) => {
        axios
            .get(process.env.VUE_APP_URL_API + '/assign/modal/table')
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

const openfileapi = (data) => {
    return new Promise((resolve, reject) => {
        axios
            .post('https://box.one.th/onebox_uploads/api/system/get/folder_from_document_no',
                {
                    document_no: data,
                    tax_id: "0107544000094",
                    branch_no: "00000"
                },
                { headers: { Authorization: 'Bearer 634f5f4c16bedd0d3dac1d9ec73c888cb5158a38dda9799acddb9c76ccfe7e26fbb44d80a104cc077a471e284704c8813ac24275111262bae6cb14a58bc8d54c' } })
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

const getchkCusFree = (data) => {
    return new Promise((resolve, reject) => {
        axios
            .get(process.env.VUE_APP_URL_API + `/chkCusFree/${data}`)
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

const addAssign = (data) => {
    return new Promise((resolve, reject) => {
        axios
            .post(process.env.VUE_APP_URL_API + '/assign/addAssign', data)
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

const editAssign = (data) => {
    return new Promise((resolve, reject) => {
        axios
            .post(process.env.VUE_APP_URL_API + '/assign/editAssign', data)
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

const delAssign = (soid, cusno) => {
    var xml = getProfile()
    var keeplog = {
        pm_id: xml.pm_id ? xml.pm_id : 'admin',
        aLog_type: 'delete',
        aLog_dtl: 'delete assign',
        aLog_to: soid,
        aLog_loc: 'so_detail'
    }
    return new Promise((resolve, reject) => {
        axios
            .put(process.env.VUE_APP_URL_API + '/assign/delAssign/', { so_id: soid, pm_id: keeplog.pm_id, keep_log: keeplog })
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

// const Getlogsdetail = (soid) => {
//   return new Promise((resolve, reject) => {
//     axios
//       .get(process.env.VUE_APP_URL_API + '/logs/detail/' + soid)
//       .then(item => {
//         resolve(item.data)
//       }).catch(err => {
//         // eslint-disable-next-line prefer-promise-reject-errors
//         reject({ ...err })
//       })
//   })
// }

// const Getfile = (data) => {
//   return new Promise((resolve, reject) => {
//     var xml = process.env.VUE_APP_URL_API + '/getfile/' + data
//     resolve(xml)
//   })
// }

const denyAutoAssign = (prm) => {
    return new Promise((resolve, reject) => {
        axios
            .post(process.env.VUE_APP_URL_API + '/assign/preload/deny', prm)
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

const acptAutoAssign = (prm) => {
    return new Promise((resolve, reject) => {
        axios
            .post(process.env.VUE_APP_URL_API + '/assign/preload/accept', prm)
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

export { getAssigntable, addAssign, delAssign, editAssign, getchkCusFree, modaltable, openfileapi, denyAutoAssign, acptAutoAssign }
