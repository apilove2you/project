import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const getProfile = (tokenuser) => {
  return new Promise((resolve, reject) => {
    axios.get(process.env.VUE_APP_URL_API + '/profile?grant_type=', { headers: { Authorization: tokenuser || getAuthToken() } }).then(item => {
      resolve(item.data)
    }).catch(err => {
      // eslint-disable-next-line prefer-promise-reject-errors
      reject({ ...err })
    })
  })
}
export { getProfile }
