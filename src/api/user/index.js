import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const getAllUser = (search, page, size) => {
  search = search || ''
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + `/pendingAPv?search=${search}&page=${page}&size=${size}`, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const saveUser = (user) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/users', user)
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const approvedUser = (id, role) => {
  return new Promise((resolve, reject) => {
    axios
      .put(process.env.VUE_APP_URL_API + '/userAPv/' + id, { role: role }, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const rejectUser = (id) => {
  return new Promise((resolve, reject) => {
    axios
      .delete(process.env.VUE_APP_URL_API + '/userAPv/' + id, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

export { getAllUser, saveUser, approvedUser, rejectUser }
