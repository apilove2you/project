import axios from 'axios'
import { getAuthToken, checkAuthToken } from '../../utils/auth'

const getLogLogin = (page, size, search, date) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/loginLogs?page=' + page + '&size=' + size + '&search=' + search + '&date=' + date, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getLogAction = (page, size, search, date) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/actionsLogs?page=' + page + '&size=' + size + '&search=' + search + '&date=' + date, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const logLogin = (data) => {
  var param = {}
  param.id = data.account_id
  param.username = data.username
  param.jwt = data.access_token
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/keepLog/login', param, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const logLogout = (data) => {
  var param = {}
  param.jwt = getAuthToken()
  param.id = data
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/keepLog/logout', param, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(e => {
        reject(e)
      })
  })
}

const logAction = (data) => {
  var param = {}
  param = data
  param.jwt = getAuthToken()
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/keepLog/actions', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(e => {
        checkAuthToken(e)
        reject(e)
      })
  })
}

export { logLogin, logLogout, logAction, getLogLogin, getLogAction }
