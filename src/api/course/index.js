import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const addCourse = (data) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/newCourse/', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err.response.data })
      })
  })
}

const editCourse = (data) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/editCourse/', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err.response.data })
      })
  })
}

const getCourse = (search, page) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getCourse/?search=' + search + '&page=' + page, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      })
  })
}

const getTeach = (id, search) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getTrainJob/' + id + '?tname=' + search, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      })
  })
}

const UploadSyllabus = (course) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/imports/pdf/course', course, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch((err) => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err.response.data })
      })
  })
}

const removeUploadSyllabus = (id, fileid) => {
  return new Promise((resolve, reject) => {
    axios
      .delete(process.env.VUE_APP_URL_API + '/pdf/course/' + id + '/' + fileid, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch((err) => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err.response.data })
      })
  })
}

export { addCourse, getCourse, editCourse, UploadSyllabus, removeUploadSyllabus, getTeach }
