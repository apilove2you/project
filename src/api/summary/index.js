import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const getSummary = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/summaryjob', { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getSummarypm = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/summary/pm')
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getSummaryservicetable = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/summary/service/table')
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getSummaryservicard = (fg = false) => {
    let jr = fg ? '?dhb=1' : ''
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/summary/service/card'+jr)
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}


export { getSummary, getSummarypm, getSummaryservicetable, getSummaryservicard }
