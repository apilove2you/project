import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const getPermission = (id) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getPermission/?user_id=' + id, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const managePermission = (page, size, search = '') => {
  search = search || ''
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/managePermission/?page=' + page + '&size=' + size + '&search=' + search, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const editPermission = (data) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/editPermission', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const newPermission = (id) => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/newPermission', {
        user_id: id,
        access_rights: [
          {
            view: true,
            add: false,
            edit: false,
            del: false,
            method: 'dashboard'
          }
        ]
      }, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

export { getPermission, managePermission, editPermission, newPermission }
