import axios from 'axios'
import { getProfile } from '@/utils/profile'

const getServicetable = () => {
    return new Promise((resolve, reject) => {
        axios
            .get(process.env.VUE_APP_URL_API + '/service/table')
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

const EditcusAtv = (data, data2) => {
    var xml = getProfile()
    var keeplog = {
        pm_id: xml.pm_id ? xml.pm_id : 'admin',
        aLog_type: 'edit',
        aLog_dtl: 'switch customer',
        aLog_to: data,
        aLog_loc: 'customer'
    }
    return new Promise((resolve, reject) => {
        axios
            .put(process.env.VUE_APP_URL_API + '/cusAtv/' + data + '/' + data2, { keep_log: keeplog })
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

const Getservicedetail = (data, data2) => {
    return new Promise((resolve, reject) => {
        axios
            .get(process.env.VUE_APP_URL_API + '/service/detail/' + data + '/' + data2)
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

const EditsoAtv = (soid, state) => {
    var xml = getProfile()
    var keeplog = {
        pm_id: xml.pm_id ? xml.pm_id : 'admin',
        aLog_type: 'edit',
        aLog_dtl: 'switch so detail',
        aLog_to: soid,
        aLog_loc: 'so_detail'
    }
    return new Promise((resolve, reject) => {
        axios
            .put(process.env.VUE_APP_URL_API + '/soAtv/' + soid + '/' + state, { keep_log: keeplog })
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

const Getlogsdetail = (soid) => {
    return new Promise((resolve, reject) => {
        axios
            .get(process.env.VUE_APP_URL_API + '/logs/detail/' + soid)
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

const Getfile = (data) => {
    return new Promise((resolve, reject) => {
        var xml = process.env.VUE_APP_URL_API + '/getfile/' + data
        resolve(xml)
    })
}

const pinLogs = (logID, state) => {
    var xml = getProfile()
    var keeplog = {
        pm_id: xml.pm_id ? xml.pm_id : 'admin',
        aLog_type: 'edit',
        aLog_dtl: 'pin log',
        aLog_to: logID,
        aLog_loc: 'slog'
    }
    return new Promise((resolve, reject) => {
        axios
            .put(process.env.VUE_APP_URL_API + '/logs/pin/' + (state ? 'n' : 'p') + '/' + logID, { keep_log: keeplog })
            .then(item => {
                resolve(item.data)
            }).catch(err => {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ ...err })
            })
    })
}

export { getServicetable, EditcusAtv, Getservicedetail, Getfile, EditsoAtv, Getlogsdetail, pinLogs }
