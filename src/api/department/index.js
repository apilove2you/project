import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const getDepart = (id, search, dept, page) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getTeam/' + id + '/web?search=' + search + '&dept=' + dept + '&page=' + page, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch((error) => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...error })
      })
  })
}

const getDropdown = (id) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getTeam/' + id + '/dept', { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getDetailDepart = (id) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/latestJob/' + id, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data.data)
      }).catch((err) => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getJobDescription = (data, id) => {
  var sent = { id: id, data: data }
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/jobDescription', sent, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

export { getDepart, getDropdown, getDetailDepart, getJobDescription }
