import axios from 'axios'
import { getProfile } from '@/utils/profile'

const getPM = (data) => {
  return new Promise((resolve, reject) => {
    var xml = data ? '/getPM?all=all' : '/getPM'
    axios
      .get(process.env.VUE_APP_URL_API + xml)
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const searchSale = (searchsaledata) => {
  return new Promise((resolve, reject) => {
    axios
      .post('https://inet-ra.inet.co.th/api/vMonk/externalApi/searchAllUser_V2', {
        search: searchsaledata,
        objectId: '',
        page: ''
      }, { headers: { authorization: 'Bearer ed4ad64cbfaf5375b5769db7323a10bcc5e3a1af50d6f556fa192525ec2fbff9b3acc84227c132a0cf896c30e11050e1ee87a1d146932309c7be163d86f24d0c04b0b7dd85cb41bbdc4340db579de58469623eb1729a312f0be0671201ad2819' } })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getPendingAPv = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/pendingAPv')
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getGuest = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/getGuest')
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const getrequestremovecus = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/request/remove/cus')
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}
const editPM = (pmData) => {
  var xml = getProfile()
  var keeplog = {
    pm_id: xml.pm_id ? xml.pm_id : 'admin',
    aLog_type: 'edit',
    aLog_dtl: 'edit pm',
    aLog_to: pmData.pm_id,
    aLog_loc: 'pm'
  }
  return new Promise((resolve, reject) => {
    axios
      .put(process.env.VUE_APP_URL_API + '/editPM/' + pmData.pm_id, {
        name: pmData.pm_name,
        nickname: pmData.pm_nickname,
        email: pmData.pm_email,
        keep_log: keeplog
      })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const addPM = (pmData) => {
  var xml = getProfile()
  var keeplog = {
    pm_id: xml.pm_id ? xml.pm_id : 'admin',
    aLog_type: 'add',
    aLog_dtl: 'add pm',
    aLog_to: '',
    aLog_loc: 'pm'
  }
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/addPM', {
        name: pmData.name,
        nickname: pmData.nickname,
        email: pmData.email,
        keep_log: keeplog
      })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const deletepm = (pmid, data) => {
  var xml = getProfile()
  var keeplog = {
    pm_id: xml.pm_id ? xml.pm_id : 'admin',
    aLog_type: 'edit',
    aLog_dtl: 'switch pm',
    aLog_to: pmid,
    aLog_loc: 'pm'
  }
  return new Promise((resolve, reject) => {
    axios
      .put(process.env.VUE_APP_URL_API + '/delpm/' + pmid + '/' + data, { keep_log: keeplog })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const userAPv = (userID, pmid) => {
  var xml = getProfile()
  var keeplog = {
    pm_id: xml.pm_id ? xml.pm_id : 'admin',
    aLog_type: 'add',
    aLog_dtl: 'add user',
    aLog_to: pmid,
    aLog_loc: 'pm'
  }
  return new Promise((resolve, reject) => {
    axios
      .put(process.env.VUE_APP_URL_API + '/userAPv/' + userID + '/' + pmid, { keep_log: keeplog })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const userReject = (userID) => {
  var xml = getProfile()
  var keeplog = {
    pm_id: xml.pm_id ? xml.pm_id : 'admin',
    aLog_type: 'delete',
    aLog_dtl: 'reject user',
    aLog_to: userID,
    aLog_loc: 'user'
  }
  return new Promise((resolve, reject) => {
    axios
      .put(process.env.VUE_APP_URL_API + '/userAPv/' + userID, { keep_log: keeplog })
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

export { getPM, getPendingAPv, addPM, userAPv, userReject, deletepm, editPM, getrequestremovecus, searchSale, getGuest }
