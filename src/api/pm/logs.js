import axios from 'axios'

const getlogsaction = (data) => {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.VUE_APP_URL_API + '/logs/action')
      .then(item => {
        resolve(item.data)
      }).catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

export { getlogsaction }
