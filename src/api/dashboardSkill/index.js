import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const objectNotNull = (obj) => {
  var newObj = []
  for (const key in obj) {
    if (obj[key]) {
      newObj.push(obj[key])
    }
  }
  return newObj.length ? newObj : ['ทั้งหมด']
}

const getSkillDashboard = (date = '', body, page, size) => {
  const fillSearch = {
    search: body.search,
    available: objectNotNull(body.available),
    skill: objectNotNull(body.skill)
  }
  return new Promise((resolve, reject) => {
    axios.post(process.env.VUE_APP_URL_API + '/skillBoard?date=' + date + '&page=' + page + '&size=' + size, fillSearch, { headers: { Authorization: getAuthToken() } }).then(item => {
      resolve(item)
    }).catch(err => {
      // eslint-disable-next-line prefer-promise-reject-errors
      reject({ ...err })
    })
  })
}
export { getSkillDashboard }
