import axios from 'axios'
import { getAuthToken } from '../../utils/auth'

const objectNotNull = obj => {
  var newObj = []
  for (const key in obj) {
    if (obj[key]) {
      newObj.push(obj[key])
    }
  }
  return newObj
}

const getStaffLeave = data => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/getStaffLeave', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const listStaffLeave = (search, site, team, leave, page, size, date, currDate) => {
  return new Promise((resolve, reject) => {
    const fillSearch = {
      leave: objectNotNull(leave),
      currDate: date,
      leaveDate: date ? null : currDate,
      site: objectNotNull(site),
      team: objectNotNull(team)
    }
    axios.post(
      process.env.VUE_APP_URL_API +
        '/listStaffLeave?page=' +
        page +
        '&size=' +
        size +
        '&search=' +
        search,
      fillSearch,
      { headers: { Authorization: getAuthToken() } }
    )
      .then(item => {
        resolve(item.data)
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const summaryLeaveModal = data => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/getStaffLeaveData', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      })
      .catch(err => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const cancelStaffLeave = data => {
//   console.log('cancel')
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/cancelStaffLeave', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch((err) => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const editLeaveDate = data => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/editLeaveDate', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch((err) => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}

const listStaffLeaveData = data => {
  return new Promise((resolve, reject) => {
    axios
      .post(process.env.VUE_APP_URL_API + '/listStaffLeaveData', data, { headers: { Authorization: getAuthToken() } })
      .then(item => {
        resolve(item.data)
      }).catch((err) => {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ ...err })
      })
  })
}
export { getStaffLeave, listStaffLeave, summaryLeaveModal, cancelStaffLeave, editLeaveDate, listStaffLeaveData }
