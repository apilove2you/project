import moment from 'moment'
import { checkHour } from '../exportExcel/timesheet'

export const template = data => {
  var timesheet = data[4]
  // var wordcut = require('wordcut')
  // wordcut.init()
  let row = ''
  for (var array in timesheet) {
    // var myArray = timesheet
    // var brk = myArray.split('')
    // var res = brk.join(' <br> ')
    // document.write('<br><br>' + res)
    var timesheetDate = moment(timesheet[array].start).format('DD/MM/YYYY')
    var worktimeDuration = moment(timesheet[array].worktime_duration, 'hhmm').format('H.mm')
    var overtimeDuration = moment(timesheet[array].overtime_duration, 'hhmm').format('H.mm')
    var totalTime = moment(checkHour(timesheet[array].worktime_duration, timesheet[array].overtime_duration), 'hhmm')
      .format('H.mm')

    row += `<tr>
        <td class="border-color" style="font-size: 11px; text-align:center; border: 1px thin #8bc34a;">` + timesheetDate + `</td>
        <td class="border-color" style="font-size: 11px; text-align:left; border: 1px thin #8bc34a; line-break: auto;">` + timesheet[array].detail + `</td>
        <td class="border-color" style="font-size: 11px; text-align:center; border: 1px thin #8bc34a;">` + timesheet[array].startTime + `</td>
        <td class="border-color" style="font-size: 11px; text-align:center; border: 1px thin #8bc34a;">` + timesheet[array].endTime + `</td>
        <td class="border-color" style="font-size: 11px; text-align:center; border: 1px thin #8bc34a;">` + worktimeDuration + `</td>
        <td class="border-color" style="font-size: 11px; text-align:center; border: 1px thin #8bc34a;">` + overtimeDuration + `</td>
        <td class="border-color" style="font-size: 11px; text-align:center; border: 1px thin #8bc34a;">` + totalTime + `</td>
        <td class="border-color" style="font-size: 11px; text-align:left; border: 1px thin #8bc34a;">` + timesheet[array].workplace + `</td>
      </tr>`
  }
  return (
    `
    <body>
      <div class="text-center">
         <br/><br/><br/><br/>
         <p>Monthly Report</p>
         <p>ประจำเดือน ` + data[3] + `</p>
         <br/><br/><br/><br/>
         <p>` + data[0] + `</p>
         <br/><br/><br/><br/>
         <p>By Internet Thailand Public Co., Ltd</p>
      </div>
      <br/><br/><br/><br/><br/><br/>
      <div class="table-container">
            <table data-pdfmake="{&quot;widths&quot;:[195,130,195]}">
                <tr>
                    <th style="text-align:center;">ผู้ดำเนินการ เจ้าหน้าที่ INET</th>
                    <th style="border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; background-color: white"></th>
                    <th style="text-align:center;">ผู้ใช้บริการ</th>
                </tr>
                <tr>
                    <td style="text-align:center;">
                    <br/>ลงชื่อ: ________________________________________ <br/>
                               (                                                    ) <br/>
                        ลงวันที่: _____________________________________
                        <p style="color: white">hide</p>
                    </td>
                    <td style="border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff"></td>
                    <td style="text-align:center;">
                    <br/>ลงชื่อ: ________________________________________ <br/>
                               (                                                    ) <br/>
                        ลงวันที่: _____________________________________
                        <p style="color: white">hide</p>
                    </td>
                </tr>
            </table>
      </div>
      <br/><br/><br/><br/>
      <div data-pdfmake="{&quot;unbreakable&quot;:true}">
        <table data-pdfmake="{&quot;widths&quot;:[120,420]}">
           <tr>
              <th class="th-text-center">ชื่อบริษัท</th>
              <td>` + data[0] + `</td>
           </tr>
           <tr>
              <th class="th-text-center">สถานที่</th>
              <td>` + data[2] + `</td>
           </tr>
           <tr>
              <th class="th-text-center">รายงานประจำเดือน</th>
              <td>` + data[3] + `</td>
           </tr>
           <tr>
              <th class="th-text-center">พนักงาน</th>
              <td>` + data[1] + `</td>
           </tr>
           <tr>
              <th class="th-text-center">ตำแหน่ง</th>
              <td>Software Engineer</td>
           </tr>
        </table>
        </div>
         <br/>
        <div>
        <table class="table-container" data-pdfmake="{&quot;widths&quot;:[45,260,30,30,30,30,30,30]}">
           <tr>
              <th style="font-size: 11px; background-color: #8bc34a; color:#ffffff; border:#8bc34a; alignment: center">วันที่</th>
              <th style="font-size: 11px; background-color: #8bc34a; color:#ffffff; border:#8bc34a; alignment: center">งานที่ได้รับมอบหมาย</th>
              <th style="font-size: 11px; background-color: #8bc34a; color:#ffffff; border:#8bc34a; alignment: center">เวลา<br>เริ่มงาน</th>
              <th style="font-size: 11px; background-color: #8bc34a; color:#ffffff; border:#8bc34a; alignment: center">เวลา<br>ออกงาน</th>
              <th style="font-size: 11px; background-color: #8bc34a; color:#ffffff; border:#8bc34a; alignment: center">ชั่วโมง<br>ทำงาน<br>ปกติ</th>
              <th style="font-size: 11px; background-color: #8bc34a; color:#ffffff; border:#8bc34a; alignment: center">ชั่วโมง<br>ทำงาน<br>ล่วงเวลา</th>
              <th style="font-size: 11px; background-color: #8bc34a; color:#ffffff; border:#8bc34a; alignment: center">รวมเวลา<br>งานทั้ง<br>หมด</th>
              <th style="font-size: 11px; background-color: #8bc34a; color:#ffffff; border:#8bc34a; alignment: center">สถานที่<br>ทำงาน</th>
           </tr>
           ${row}
        </table>
        </div>
    </body>
  `
  )
}
