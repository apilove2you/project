import { template } from './template'
import moment from 'moment'
import { headerImg, footerImg } from './imageBase64'

var pdfMake = require('pdfmake/build/pdfmake')
var pdfFonts = require('./vfs_fonts')
pdfMake.vfs = pdfFonts.pdfMake.vfs
var htmlToPdfMake = require('html-to-pdfmake')
const publicStyle = require('./style.js')

// ต้องระบุตามชื่อของ ไฟล์ font
pdfMake.fonts = {
  THSarabun: {
    normal: 'Sarabun-Light.ttf',
    bold: 'Sarabun-SemiBold.ttf',
    italics: 'Sarabun-Italic.ttf',
    bolditalics: 'Sarabun-BoldItalic.ttf'
  },
  Fontello: {
    normal: 'fontello.ttf',
    bold: 'fontello.ttf',
    italics: 'fontello.ttf',
    bolditalics: 'fontello.ttf'
  }
}
export const exportPdfTimesheet = async (obj, refs) => {
  try {
    var monthsen = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ]
    var monthsth = [
      'มกราคม',
      'กุมภาพันธ์',
      'มีนาคม',
      'เมษายน',
      'พฤษภาคม',
      'มิถุนายน',
      'กรกฎาคม',
      'สิงหาคม',
      'กันยายน',
      'ตุลาคม',
      'พฤศจิกายน',
      'ธันวาคม'
    ]
    var month = monthsen.indexOf(refs.calendar.title.split(' ')[0])

    if (month === -1) {
      month = monthsth.indexOf(refs.calendar.title.split(' ')[0])
    }
    month = month + 1
    var getday = new Date(refs.calendar.title.split(' ')[1], month, 0).getDate()

    var changeMonth = moment(
      monthsen[month - 1] + ' ' + refs.calendar.title.split(' ')[1]
    )
      .locale('th')
      .add(543, 'year')
      .format('LL')
    var MonthThai = changeMonth.slice(2, changeMonth.length)
    const data = []
    const data2 = [
      obj.company_name,
      obj.staff_name,
      obj.staffjob_obj.address_onsite || '-'
    ]
    const res = []
    for (var i = 0; i < getday; i++) {
      for (var array in obj.result) {
        var ArrayDate = i + 1
        var DateExcel =
        refs.calendar.title.split(' ')[1].toString() +
        '-' +
        month.toString() +
        '-' +
        ArrayDate.toString()
        var DateEnd = obj.result[array].start
        if (
          new Date(DateExcel).toDateString() === new Date(DateEnd).toDateString()
        ) {
          data.push(obj.result[array])
        } else {
        }
      }
    }

    for (var array2 in data2) {
      res.push(data2[array2])
    }

    res.push(MonthThai, data)
    var html = htmlToPdfMake(template(res))
    var header = headerImg()
    var footer = footerImg()

    var docDefinition = {
      pageSize: 'A4',
      pageOrientation: 'portrait',
      pageMargins: [20, 120, 20, 70],
      header: {
        image: header,
        width: 600,
        height: 100
      },
      content: [html],
      footer:
    function (pageCount, footerImg) {
      return {
        margin: [80, 0],
        columns: [
          {
            image: footer,
            width: 380,
            height: 55,
            alignment: 'center'
          },
          {
            fontSize: 12,
            text: { text: pageCount },
            alignment: 'right'
          }
        ]
      }
    },
      styles: {
        ...publicStyle
      },
      defaultStyle: {
        font: 'THSarabun'
      }
    }
    var pdfDocGenerator = pdfMake.createPdf(docDefinition)
    pdfDocGenerator.open()
  } catch (error) {
  }
}
