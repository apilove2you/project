module.exports = {
  'text-center': {
    alignment: 'center',
    fontSize: 24,
    bold: true
  },
  'th-text-center': {
    alignment: 'center',
    bold: true
  },
  'border-color': {
    border: [true, true, true, true],
    borderColor: ['red', 'red', 'red', 'red'],
    layout: {
      fillColor: function (rowIndex, node, columnIndex) {
        return (rowIndex % 2 === 0) ? '#CCCCCC' : null
      }
    }
  }
}
