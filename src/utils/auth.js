import decode from 'jwt-decode'
import axios from 'axios'
import { getProfile, clearProfile } from './profile'

const AUTH_TOKEN_KEY = 'access_token'
const AUTH_TOKEN_KEY_ONE = 'one_token'

function removeA (arr) {
  var what; var a = arguments; var L = a.length; var ax
  while (L > 1 && arr.length) {
    what = a[--L]
    while ((ax = arr.indexOf(what)) !== -1) {
      arr.splice(ax, 1)
    }
  }
  return arr
}

function logoutUser () {
  clearAuthToken()
}

function checkAuthToken (apierror) {
  if (apierror.response.status === 401) {
    window.$cookies.remove('inetoutsource')
    clearAuthToken()
    clearProfile()
    window.location.href = window.location.protocol + '//' + window.location.host + '/' + '?errortoken=extoken'
  }
}

function getCookies () {
  return window.$cookies.get('inetoutsource')
}

function setAuthToken (token, onetoken) {
  localStorage.setItem(AUTH_TOKEN_KEY, token)
  localStorage.setItem(AUTH_TOKEN_KEY_ONE, onetoken)
}

function getAuthToken () {
  return localStorage.getItem(AUTH_TOKEN_KEY)
}

function clearAuthToken () {
  axios.defaults.headers.common.Authorization = ''
  localStorage.removeItem(AUTH_TOKEN_KEY)
}

function isLoggedIn () {
  const authToken = getAuthToken()
  return !!authToken && !isTokenExpired(authToken)
}

function getTokenExpirationDate (encodedToken) {
  const token = decode(encodedToken)
  if (!token.exp) {
    return null
  }

  const date = new Date(0)
  date.setUTCSeconds(token.exp)

  return date
}

function isTokenExpired (token) {
  const expirationDate = getTokenExpirationDate(token)
  return expirationDate < new Date() || !token
}

function isAdmin () {
  const profile = getProfile()
  if (profile) {
    return profile.role === 'Admin'
  } else {
    return false
  }
}

function isSuperUser () {
  const profile = getProfile()
  if (profile) {
    return profile.role === 'SuperUser'
  } else {
    return false
  }
}

function getCheckAdminDraw (drawer) {
  const pageAdmin = [
    '/dashboard',
    '/skilldashboard',
    '/staff',
    '/staffleave',
    '/summary',
    '/job',
    // ----- timesheet -----
    '/timesheetmanagejob',
    '/timesheetmanagestaffs',
    // ----- management -----
    '/usermanagement',
    '/skillmanagement',
    '/coursemanagement',
    // ----- logs -----
    '/login',
    '/action',
    '/permissionmanagement',
    // '/transferPermission'
  ]
  var pageSuperUser = [
    '/dashboard',
    '/skilldashboard',
    '/staff',
    '/staffleave',
    '/summary',
    '/job',
    // ----- timesheet -----
    '/timesheetmanagejob',
    '/timesheetmanagestaffs',
    // ----- logs -----
    '/login',
    '/action',
    // ----- management -----
    '/coursemanagement',

    '/timesheet',
    // '/transferPermission'
  ]
  const pageUser = [
    '/timesheet',
    '/profile',
    '/project'
  ]
  if (getProfile().staff_id === null) { removeA(pageSuperUser, '/timesheet') }
  if (isAdmin()) {
    const result = []
    // return drawer.filter(item => pageSuperUser.indexOf(item.link) !== -1)
    for (let i = 0; i < drawer.length; i++) {
      const item = drawer[i]
      if (!item.items) {
        if (pageAdmin.indexOf(item.link) !== -1) {
          result.push(item)
        }
      } else {
        const items = []
        for (let j = 0; j < item.items.length; j++) {
          const val = item.items[j]
          const splitDrawerSub = val.link.split('/')
          if (pageAdmin.indexOf('/' + splitDrawerSub[splitDrawerSub.length - 1]) !== -1) {
            items.push(val)
          }
        }
        if (items.length) {
          result.push({
            ...item,
            items: items
          })
        }
      }
    }
    return result
  } else if (isSuperUser()) {
    var inetoutsourcecookies = getCookies()
    var Arraypagesuperuser = []
    if (inetoutsourcecookies) {
      for (var indexxml in inetoutsourcecookies.access_rights) {
        if (inetoutsourcecookies.access_rights[indexxml].view === true) {
          if (inetoutsourcecookies.access_rights[indexxml].method === 'staffdev' || inetoutsourcecookies.access_rights[indexxml].method === 'staffinfrastructor') {
            Arraypagesuperuser.push('/staff')
          } else {
            Arraypagesuperuser.push('/' + inetoutsourcecookies.access_rights[indexxml].method)
          }
        }
      }
    }
    pageSuperUser = Arraypagesuperuser
    const result = []
    // return drawer.filter(item => pageSuperUser.indexOf(item.link) !== -1)
    for (let i = 0; i < drawer.length; i++) {
      const item = drawer[i]
      if (!item.items) {
        if (pageSuperUser.indexOf(item.link) !== -1) {
          result.push(item)
        }
      } else {
        const items = []
        for (let j = 0; j < item.items.length; j++) {
          const val = item.items[j]
          const splitDrawerSub = val.link.split('/')
          if (pageSuperUser.indexOf('/' + splitDrawerSub[splitDrawerSub.length - 1]) !== -1) {
            items.push(val)
          }
        }
        if (items.length) {
          result.push({
            ...item,
            items: items
          })
        }
      }
    }
    return result
  } else {
    const result = []
    // return drawer.filter(item => pageSuperUser.indexOf(item.link) !== -1)
    for (let i = 0; i < drawer.length; i++) {
      const item = drawer[i]
      if (!item.items) {
        if (pageUser.indexOf(item.link) !== -1) {
          result.push(item)
        }
      } else {
        const items = []
        for (let j = 0; j < item.items.length; j++) {
          const val = item.items[j]
          const splitDrawerSub = val.link.split('/')
          if (pageUser.indexOf('/' + splitDrawerSub[splitDrawerSub.length - 1]) !== -1) {
            items.push(val)
          }
        }
        if (items.length) {
          result.push({
            ...item,
            items: items
          })
        }
      }
    }
    return result
  }
}

export {
  isLoggedIn,
  clearAuthToken,
  logoutUser,
  setAuthToken,
  getAuthToken,
  isAdmin,
  isSuperUser,
  getCheckAdminDraw,
  removeA,
  getCookies,
  checkAuthToken
}
