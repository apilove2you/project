import swal from 'sweetalert2'

const TopSwal = (title, icon) => {
  const Toast = swal.mixin({
    padding: '30px',
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', swal.stopTimer)
      toast.addEventListener('mouseleave', swal.resumeTimer)
    }
  })
  Toast.fire({
    icon: `${icon}`,
    title: `${title}`
  })
  return Toast
}

const ConfirmSwal = (title, detail) => {
  const Toast = swal.fire({
    title: title,
    text: detail,
    showCancelButton: true,
    confirmButtonColor: '#5aa051',
    cancelButtonColor: '#acb0b1',
    confirmButtonText: 'ยืนยัน',
    cancelButtonText: 'ปิด'
  }).then((result) => {
    if (result.isConfirmed) {
      return true
    } else {
      return false
    }
  })
  return Toast
}
export { TopSwal, ConfirmSwal }
