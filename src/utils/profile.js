import { logLogout } from '../api/log'

const setProfile = (profile) => {
  const user = {
    ...profile.user,
    username: profile.username
  }
  localStorage.setItem('PROFILE', JSON.stringify(user))
}

const getProfile = () => {
  const profile = localStorage.getItem('PROFILE')
  return JSON.parse(profile)
}

const clearProfile = async () => {
  var profile = getProfile()
  localStorage.removeItem('PROFILE')
  if (profile.account_id) {
    await logLogout(profile.account_id)
  }
}

export {
  getProfile,
  setProfile,
  clearProfile
}
