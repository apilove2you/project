import FileSaver from 'file-saver'
import XLSX from 'xlsx-style'
import { defaultCellStyle, headerCellStyle, evenRowStyle } from './options'
import moment from 'moment'

export const exportExcelTimesheet = (obj, refs) => {
  var monthsen = ['January', 'February', 'March', 'April', 'May',
    'June', 'July', 'August', 'September',
    'October', 'November', 'December'
  ]
  var monthsth = [
    'มกราคม',
    'กุมภาพันธ์',
    'มีนาคม',
    'เมษายน',
    'พฤษภาคม',
    'มิถุนายน',
    'กรกฎาคม',
    'สิงหาคม',
    'กันยายน',
    'ตุลาคม',
    'พฤศจิกายน',
    'ธันวาคม'
  ]
  var month = monthsen.indexOf(refs.calendar.title.split(' ')[0])

  if (month === -1) {
    month = monthsth.indexOf(refs.calendar.title.split(' ')[0])
  }
  month = month + 1

  var getday = new Date(refs.calendar.title.split(' ')[1], month, 0).getDate()
  // alert(getday)
  var changeMonth = moment(
    monthsen[month - 1] + ' ' + refs.calendar.title.split(' ')[1]
  )
    .locale('th')
    .add(543, 'year')
    .format('LL')
  var MonthThai = changeMonth.slice(2, changeMonth.length)
  const data = [
    [
      'บันทึกการปฏิบัติงานประจำของพนักงาน INET',
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    ],
    ['เดือน' + ' ' + MonthThai, null, null, null, null, null, null, null, null],
    [null],
    [
      null,
      'ชื่อผู้ปฏิบัติงาน:',
      null,
      obj.staff_obj.fname + ' ' + obj.staff_obj.lname
    ],
    [null, 'ตำแหน่งงาน:', null, 'Software Engineer'],
    [null, 'ชื่อโปรเจกต์ที่ทำ:', null, obj.project_name || ''],
    [null, 'ทักษะที่ไปทำ:', null, obj.staffjob_obj.skill_type || ''],
    [null, 'ชื่อผู้ดูแลโปรเจกต์:', null, obj.supervisor],
    [null, 'ติดต่อผู้ดูแลโปรเจกต์:', null, obj.supervisor_contact],
    [null],
    [
      'วันที่',
      'งานที่ได้รับมอบหมาย',
      'เวลาเริ่มงาน',
      'เวลาออกงาน',
      'ชั่วโมงทำงานปกติ',
      'ชั่วโมงทำงานล่วงเวลา',
      'รวมเวลาทำงานทั้งหมด',
      'สถานที่ทำงาน'
    ]
  ]
  const data2 = [
    [
      null,
      'ผู้ใช้บริการ',
      null,
      'ผู้ปฏิบัติงาน',
      null,
      null,
      null,
      null,
      null,
      null,
      null
    ],
    [
      null,
      obj.company_name,
      null,
      obj.staff_obj.fname + ' ' + obj.staff_obj.lname,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    ],
    [
      null,
      null,
      null,
      'Software Engineer',
      null,
      null,
      null,
      null,
      null,
      null,
      null
    ]
  ]
  for (var i = 0; i < getday; i++) {
    var number = 0
    for (var array in obj.result) {
      var ArrayDate = i + 1
      var DateExcel =
        refs.calendar.title.split(' ')[1].toString() +
        '-' +
        month.toString() +
        '-' +
        ArrayDate.toString()
      var worktimeDuration =
        moment(obj.result[array].worktime_duration, 'hhmm').format('H.mm') +
        ' ชม.'
      var overtimeDuration =
        moment(obj.result[array].overtime_duration, 'hhmm').format('H.mm') +
        ' ชม.'
      var totalTime =
        moment(
          checkHour(
            obj.result[array].worktime_duration,
            obj.result[array].overtime_duration
          ),
          'hhmm'
        ).format('H.mm') + ' ชม.'
      if (
        new Date(DateExcel).toDateString() ===
        new Date(obj.result[array].end).toDateString()
      ) {
        data.push([
          ArrayDate || '',
          obj.result[array].detail || '',
          obj.result[array].startTime || '',
          obj.result[array].endTime || '',
          worktimeDuration || '',
          overtimeDuration || '',
          totalTime || '',
          obj.result[array].workplace || ''
        ])
        number = 1
      } else {
      }
    }
    if (number === 0) {
      data.push([i + 1, ' ', ' ', ' ', ' ', ' ', ' ', ' '])
    }
  }
  data.push([])

  for (var array2 in data2) {
    data.push(data2[array2])
  }
  // json => sheet
  const sheet = sheet_from_array_of_arrays(data, data2)
  // Header merge: r: row row; c: column column
  const mergeTitle = [
    { s: { r: 0, c: 0 }, e: { r: 0, c: 7 } },
    { s: { r: 1, c: 0 }, e: { r: 1, c: 7 } },
    { s: { r: 3, c: 1 }, e: { r: 3, c: 2 } },
    { s: { r: 3, c: 3 }, e: { r: 3, c: 6 } },
    { s: { r: 4, c: 1 }, e: { r: 4, c: 2 } },
    { s: { r: 4, c: 3 }, e: { r: 4, c: 6 } },
    { s: { r: 5, c: 1 }, e: { r: 5, c: 2 } },
    { s: { r: 5, c: 3 }, e: { r: 5, c: 6 } },
    { s: { r: 6, c: 1 }, e: { r: 6, c: 2 } },
    { s: { r: 6, c: 3 }, e: { r: 6, c: 6 } },
    { s: { r: 7, c: 1 }, e: { r: 7, c: 2 } },
    { s: { r: 7, c: 3 }, e: { r: 7, c: 6 } },
    { s: { r: 8, c: 1 }, e: { r: 8, c: 2 } },
    { s: { r: 8, c: 3 }, e: { r: 8, c: 6 } },
    { s: { r: 10, c: 0 }, e: { r: 10, c: 0 } },
    { s: { r: 11, c: 1 }, e: { r: 11, c: 1 } },
    { s: { r: 12, c: 1 }, e: { r: 12, c: 1 } },
    { s: { r: 13, c: 2 }, e: { r: 13, c: 2 } },
    { s: { r: 14, c: 2 }, e: { r: 14, c: 2 } },
    { s: { r: 15, c: 2 }, e: { r: 15, c: 2 } },
    { s: { r: 16, c: 2 }, e: { r: 16, c: 2 } },
    { s: { r: 17, c: 2 }, e: { r: 17, c: 2 } },
    { s: { r: 18, c: 2 }, e: { r: 18, c: 2 } },
    { s: { r: 19, c: 2 }, e: { r: 19, c: 2 } },
    { s: { r: 20, c: 2 }, e: { r: 20, c: 2 } },
    { s: { r: 21, c: 2 }, e: { r: 21, c: 2 } },
    { s: { r: 22, c: 2 }, e: { r: 22, c: 2 } },
    { s: { r: 23, c: 2 }, e: { r: 23, c: 2 } },
    { s: { r: 24, c: 2 }, e: { r: 24, c: 2 } },
    { s: { r: 25, c: 2 }, e: { r: 25, c: 2 } },
    { s: { r: 26, c: 2 }, e: { r: 26, c: 2 } },
    { s: { r: 27, c: 2 }, e: { r: 27, c: 2 } },
    { s: { r: 28, c: 2 }, e: { r: 28, c: 2 } },
    { s: { r: 29, c: 2 }, e: { r: 29, c: 2 } },
    { s: { r: 30, c: 2 }, e: { r: 30, c: 2 } },
    { s: { r: 31, c: 2 }, e: { r: 31, c: 2 } },
    { s: { r: 32, c: 2 }, e: { r: 32, c: 2 } },
    { s: { r: 33, c: 2 }, e: { r: 33, c: 2 } },
    { s: { r: 34, c: 2 }, e: { r: 34, c: 2 } },
    { s: { r: 35, c: 2 }, e: { r: 35, c: 2 } },
    { s: { r: 36, c: 2 }, e: { r: 36, c: 2 } },
    { s: { r: 37, c: 2 }, e: { r: 37, c: 2 } },
    { s: { r: 38, c: 2 }, e: { r: 38, c: 2 } },
    { s: { r: 39, c: 2 }, e: { r: 39, c: 2 } },
    { s: { r: 40, c: 2 }, e: { r: 40, c: 2 } },
    { s: { r: 41, c: 2 }, e: { r: 41, c: 2 } },
    { s: { r: 42, c: 1 }, e: { r: 42, c: 2 } },
    { s: { r: 42, c: 3 }, e: { r: 42, c: 4 } },
    { s: { r: 43, c: 1 }, e: { r: 43, c: 2 } },
    { s: { r: 43, c: 3 }, e: { r: 43, c: 4 } },
    { s: { r: 44, c: 1 }, e: { r: 44, c: 2 } },
    { s: { r: 44, c: 3 }, e: { r: 44, c: 4 } },
    { s: { r: 45, c: 1 }, e: { r: 45, c: 2 } },
    { s: { r: 45, c: 3 }, e: { r: 45, c: 4 } },
    { s: { r: 46, c: 1 }, e: { r: 46, c: 2 } },
    { s: { r: 46, c: 3 }, e: { r: 46, c: 4 } }
  ]
  // sheet['!merges'] = mergeTitle.concat(mergeContent);
  sheet['!merges'] = mergeTitle
  // Freeze the first 6 rows and the first column, you can slide the bottom right
  sheet['!freeze'] = {
    xSplit: '1',
    ySplit: '6',
    topLeftCell: 'B7',
    activePane: 'bottomRight',
    state: 'frozen'
  }
  sheet['!margins'] = {
    left: 1.0,
    right: 1.0,
    top: 1.0,
    bottom: 1.0,
    header: 0.5,
    footer: 0.5
  }
  // column width is not pixel value
  const sheetCols = [
    { wch: 15 }, // serial number
    { wch: 50 }, // City
    { wch: 15 }, // Demonstration project name
    { wch: 15 }, // intermodal line (pieces)
    { wch: 15 }, // Line
    { wch: 15 }, // Intermodal route
    { wch: 15 }, // Combined transport mode
    { wch: 15 }, // Combined transport volume-ten thousand tons
    { wch: 15 }, // Combined transport volume-ten thousand standard container
    { wch: 15 }, // Combined transport turnover
    { wch: 15 }, // Enterprise ten thousand tons
    { wch: 15 }, // Enterprise million standard box
    { wch: 15 }, // Hub station
    { wch: 15 }, // equipment
    { wch: 15 }, // Informationization
    { wch: 15 } // Remarks
  ]
  sheet['!cols'] = sheetCols
  addRangeBorder(mergeTitle, sheet)

  const wbBlob = sheet2blob(sheet, '1')
  // save download
  FileSaver.saveAs(
    wbBlob,
    `${obj.staff_obj.id}_${obj.staff_obj.fname}_${obj.staff_obj.lname}.xlsx`
  )
}

// eslint-disable-next-line camelcase
const sheet_from_array_of_arrays = (data) => {
  const headerTxt = [
    'บันทึกการปฏิบัติงานประจำของพนักงาน INET',
    'บันทึกการปฏิบัติงานประจำขอ',
    'ชื่อผู้ปฏิบัติงาน:',
    'ตำแหน่งงาน:',
    'ชื่อโปรเจกต์ที่ทำ:',
    'ทักษะที่ไปทำ:',
    'ชื่อผู้ดูแลโปรเจกต์:',
    'ติดต่อผู้ดูแลโปรเจกต์:',
    'วันที่',
    'งานที่ได้รับมอบหมาย',
    'เวลาเริ่มงาน',
    'เวลาออกงาน',
    'ชั่วโมงทำงานปกติ',
    'ชั่วโมงทำงานล่วงเวลา',
    'รวมเวลาทำงานทั้งหมด',
    'สถานที่ทำงาน',
    'ผู้ใช้บริการ',
    'ผู้ปฏิบัติงาน'
  ]
  const ws = {}
  const range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } }
  for (let R = 0; R !== data.length; ++R) {
    for (let C = 0; C !== data[R].length; ++C) {
      if (range.s.r > R) range.s.r = R
      if (range.s.c > C) range.s.c = C
      if (range.e.r < R) range.e.r = R
      if (range.e.c < C) range.e.c = C
      /// When the cell is generated here, the default style defined above is used
      const cell = { v: data[R][C], s: defaultCellStyle }
      if (cell.v == null) continue
      if (R > 10 && R < 42 && R % 2 === 0) {
        cell.s = evenRowStyle
      }
      if (headerTxt.includes(cell.v)) {
        cell.s = headerCellStyle
      }
      const cellref = XLSX.utils.encode_cell({ c: C, r: R })

      /* TEST: proper cell types and value handling */
      if (typeof cell.v === 'number') cell.t = 'n'
      else if (typeof cell.v === 'boolean') cell.t = 'b'
      else if (cell.v instanceof Date) {
        cell.t = 'n'
        cell.z = XLSX.SSF._table[14]
        cell.v = this.dateNum(cell.v)
      } else cell.t = 's'
      ws[cellref] = cell
    }
  }
  if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range)
  return ws
}

const sheet2blob = (sheet, sheetName) => {
  sheetName = sheetName || 'sheet1'
  const workbook = {
    SheetNames: [sheetName],
    Sheets: {}
  }
  workbook.Sheets[sheetName] = sheet
  // Generate configuration items for excel
  const wopts = {
    bookType: 'xlsx', // file type to be generated
    bookSST: false, // Whether to generate Shared String Table, the official explanation is that if it is turned on, the generation speed will decrease, but there is better compatibility on lower version IOS devices
    type: 'binary'
  }
  const wbout = XLSX.write(workbook, wopts, {
    defaultCellStyle: defaultCellStyle
  })
  const blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' })
  // String to ArrayBuffer
  function s2ab (s) {
    const buf = new ArrayBuffer(s.length)
    const view = new Uint8Array(buf)
    for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xff
    return buf
  }
  return blob
}

const addRangeBorder = (range, ws) => {
  const arr = [
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z'
  ]

  range.forEach(item => {
    const startColNumber = Number(item.s.r)
    const endColNumber = Number(item.e.r)
    const startRowNumber = Number(item.s.c)
    const endRowNumber = Number(item.e.c)
    const test = ws[arr[startRowNumber] + (startColNumber + 1)]
    for (let col = startColNumber; col <= endColNumber; col++) {
      for (let row = startRowNumber; row <= endRowNumber; row++) {
        ws[arr[row] + (col + 1)] = test
        // {s:{border:{top:{style:'thin'}, left:{style:'thin'},bottom:{style:'thin'},right:{style:'thin'}}}};
      }
    }
  })
  return ws
}

export const checkHour = (worktime, overtime) => {
  const workHourArr = worktime ? worktime.split(':') : '00:00'.split(':')
  const overtimeHourArr = overtime ? overtime.split(':') : '00:00'.split(':')
  var hour = parseInt(workHourArr[0]) + parseInt(overtimeHourArr[0])
  var minute = parseInt(workHourArr[1]) + parseInt(overtimeHourArr[1])
  hour = hour + Math.floor(minute / 60)
  minute = minute % 60
  return (
    (hour < 10 ? '0' + hour : hour) +
    ':' +
    (minute < 10 ? '0' + minute : minute)
  )
}

export const exportStaffs = (obj) => {
  const data = [
    [
      'CV',
      'รหัสพนักงาน',
      'ชื่อ',
      'นามสกุล',
      'ชื่อเล่น',
      'ศูนย์',
      'ทีม',
      'วันที่เริ่มงาน',
      'ประเภทงาน',
      'สถานะพนักงาน',
      'Outsource',
      'โปรเจกต์ที่กำลังทำ',
      'วันที่เริ่มโปรเจกต์',
      'วันที่สิ้นสุดโปรเจกต์',
      'ที่พัก',
      'รูปแบบงาน',
      'หมายเหตุ'
    ]
  ]
  for (var array in obj) {
    var cv = 'ไม่มี'
    var starDate = moment(obj[array].start_date).format('YYYY-MM-DD')
    var startJobsDate = moment(obj[array].start_jobs_date).format('YYYY-MM-DD')
    var finishJobsDate = moment(obj[array].finish_jobs_date).format('YYYY-MM-DD')
    data.push([
      cv || '',
      obj[array].id || '',
      obj[array].fname || '',
      obj[array].lname || '',
      obj[array].nname || '',
      obj[array].center || '',
      obj[array].team || '',
      starDate || '',
      obj[array].status || '',
      obj[array].available || '',
      obj[array].outsource || '',
      obj[array].matchjob || '',
      startJobsDate || '',
      finishJobsDate || '',
      obj[array].address_onsite || '',
      obj[array].status_site || '',
      obj[array].note || ''
    ])
  }
  data.push([])

  const sheet = sheet_from_array_of_arrays(data)

  const wbBlob = sheet2blob(sheet, '1')

  // save download
  FileSaver.saveAs(
    wbBlob,
    'staff-list.xlsx'
  )
}
