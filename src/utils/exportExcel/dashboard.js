import FileSaver from 'file-saver'
import XLSX from 'xlsx-style'
import { defaultCellStyle } from './options'

function servCount(key, serv, state) {
    if (state == 1) {
        var val = key.services.find((s) => s.service === serv)
    }
    else {
        var val = key.so_status.find((s) => s.status === serv)
    }
    return val ? val.count : 0
}

export const exportDashboard = (...arr) => {

    const allServ = arr[0]
    const data1 = [
        ["PM Name", "All PM", "Active PM", "MNSP", "BCP", "Report", "Critical", "Summary"]
    ]
    allServ.forEach(key => {
        data1.push([
            key.pm_name,
            servCount(key, "all_pm", 1),
            servCount(key, "pm", 1),
            servCount(key, "mnsp", 1),
            servCount(key, "bcp", 1),
            servCount(key, "report", 1),
            servCount(key, "cri", 1),
            key.total,
        ])
    })
    data1.push([])

    const data2 = [
        arr[1].map(a => a.name),
    ]
    data2[0][0] = "PM Name"
    data2[0].push("Summary")
    const allPM = arr[2]
    allPM.forEach(key => {
        data2.push([
            key.pm_name,
            servCount(key, "complete", "2"),
            servCount(key, "in_progress", "2"),
            servCount(key, "overdue", "2"),
            servCount(key, "uat", "2"),
            servCount(key, "waiting_customer", "2"),
            servCount(key, "waiting_so", "2"),
            servCount(key, "terminate", "2"),
            servCount(key, "cancel", "2"),
            key.total,
        ])
    })
    var total = arr[1].map(a => a.value)
    let temp = total[0] // summary
    total[0] = "Total"
    total.push(temp)
    data2.push(
        total,
        []
    )

    const sheet1 = sheet_from_array_of_arrays(data1)
    const sheet2 = sheet_from_array_of_arrays(data2)
    const wbBlob = sheet2blob(sheet1, sheet2)

    // save download
    FileSaver.saveAs(
        wbBlob,
        'summary-Service.xlsx'
    )
}

// eslint-disable-next-line camelcase
const sheet_from_array_of_arrays = data => {
    const ws = {}
    const range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } }
    for (let R = 0; R !== data.length; ++R) {
        for (let C = 0; C !== data[R].length; ++C) {
            if (range.s.r > R) range.s.r = R
            if (range.s.c > C) range.s.c = C
            if (range.e.r < R) range.e.r = R
            if (range.e.c < C) range.e.c = C
            /// When the cell is generated here, the default style defined above is used
            const cell = { v: data[R][C], s: defaultCellStyle }
            if (cell.v == null) continue
            const cellref = XLSX.utils.encode_cell({ c: C, r: R })

            /* TEST: proper cell types and value handling */
            if (typeof cell.v === 'number') cell.t = 'n'
            else if (typeof cell.v === 'boolean') cell.t = 'b'
            else if (cell.v instanceof Date) {
                cell.t = 'n'
                cell.z = XLSX.SSF._table[14]
                cell.v = this.dateNum(cell.v)
            } else cell.t = 's'
            ws[cellref] = cell
        }
    }
    if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range)
    ws['!cols'] = [
        { wch: 50 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 20 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
    ]
    return ws
}

const sheet2blob = (sheet1, sheet2) => {
    const workbook = {
        SheetNames: ["All Service", "PM Service"],
        Sheets: {}
    }
    workbook.Sheets["All Service"] = sheet1
    workbook.Sheets["PM Service"] = sheet2
    // Generate configuration items for excel
    const wopts = {
        bookType: 'xlsx', // file type to be generated
        bookSST: false, // Whether to generate Shared String Table, the official explanation is that if it is turned on, the generation speed will decrease, but there is better compatibility on lower version IOS devices
        type: 'binary'
    }
    const wbout = XLSX.write(workbook, wopts, {
        defaultCellStyle: defaultCellStyle
    })
    const blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' })
    // String to ArrayBuffer
    function s2ab(s) {
        const buf = new ArrayBuffer(s.length)
        const view = new Uint8Array(buf)
        for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xff
        return buf
    }
    return blob
}
