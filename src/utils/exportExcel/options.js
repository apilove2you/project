export const defaultCellStyle = {
  font: {
    name: ' ', sz: 11, color: { auto: 1 }
  },
  border: {
    color: { auto: 1 },
    top: { style: 'thin' },
    bottom: { style: 'thin' },
    left: { style: 'thin' },
    right: { style: 'thin' }
  },
  alignment: {
    /// wrap
    wrapText: 1,
    // centered
    horizontal: 'center',
    vertical: 'center',
    indent: 0
  }
}

export const headerCellStyle = {
  font: {
    name: ' ', sz: 11, color: { auto: 1 }
  },
  border: {
    color: { auto: 1 },
    top: { style: 'thin' },
    bottom: { style: 'thin' },
    left: { style: 'thin' },
    right: { style: 'thin' }
  },
  alignment: {
    /// wrap
    wrapText: 1,
    // centered
    horizontal: 'center',
    vertical: 'center',
    indent: 0
  },
  fill: {
    fgColor: { rgb: '70AD47' }
  }
}

export const evenRowStyle = {
  font: {
    name: ' ', sz: 11, color: { auto: 1 }
  },
  border: {
    color: { auto: 1 },
    top: { style: 'thin' },
    bottom: { style: 'thin' },
    left: { style: 'thin' },
    right: { style: 'thin' }
  },
  alignment: {
    /// wrap
    wrapText: 1,
    // centered
    horizontal: 'center',
    vertical: 'center',
    indent: 0
  },
  fill: {
    fgColor: { rgb: 'E2EFD9' }
  }
}
