import FileSaver from 'file-saver'
import XLSX from 'xlsx-style'
import { defaultCellStyle } from './options'
import moment from 'moment'

export const exportService = (arr) => {
    const data = [
        [
            'Customer ID',
            'Customer Name',
            'PM Name',
            'SO No.',
            'Service',
            'Status',
        ]
    ]
    arr.forEach(key => {
        key.so_no.forEach(so => {
            data.push([
                key.cus_id,
                key.cus_name,
                so.pm_name,
                so.so_no,
                so.services.join().toUpperCase(),
                so.so_status
            ])
        })
    })
    data.push([])

    const sheet = sheet_from_array_of_arrays(data)

    const wbBlob = sheet2blob(sheet, '1')

    // save download
    FileSaver.saveAs(
        wbBlob,
        'all-Service.xlsx'
    )
}

// eslint-disable-next-line camelcase
const sheet_from_array_of_arrays = data => {
    const ws = {}
    const range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } }
    for (let R = 0; R !== data.length; ++R) {
        for (let C = 0; C !== data[R].length; ++C) {
            if (range.s.r > R) range.s.r = R
            if (range.s.c > C) range.s.c = C
            if (range.e.r < R) range.e.r = R
            if (range.e.c < C) range.e.c = C
            /// When the cell is generated here, the default style defined above is used
            const cell = { v: data[R][C], s: defaultCellStyle }
            if (cell.v == null) continue
            const cellref = XLSX.utils.encode_cell({ c: C, r: R })

            /* TEST: proper cell types and value handling */
            if (typeof cell.v === 'number') cell.t = 'n'
            else if (typeof cell.v === 'boolean') cell.t = 'b'
            else if (cell.v instanceof Date) {
                cell.t = 'n'
                cell.z = XLSX.SSF._table[14]
                cell.v = this.dateNum(cell.v)
            } else cell.t = 's'
            ws[cellref] = cell
        }
    }
    if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range)
    ws['!cols'] = [
        { wch: 15 }, // 'Customer ID'
        { wch: 50 }, // 'Customer Name'
        { wch: 30 }, // 'PM Name'
        { wch: 30 }, // 'SO No.'
        { wch: 30 }, // 'Service'
        { wch: 15 }, // 'Status'
    ]
    return ws
}

const sheet2blob = (sheet, sheetName) => {
    sheetName = sheetName || 'sheet1'
    const workbook = {
        SheetNames: [sheetName],
        Sheets: {}
    }
    workbook.Sheets[sheetName] = sheet
    // Generate configuration items for excel
    const wopts = {
        bookType: 'xlsx', // file type to be generated
        bookSST: false, // Whether to generate Shared String Table, the official explanation is that if it is turned on, the generation speed will decrease, but there is better compatibility on lower version IOS devices
        type: 'binary'
    }
    const wbout = XLSX.write(workbook, wopts, {
        defaultCellStyle: defaultCellStyle
    })
    const blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' })
    // String to ArrayBuffer
    function s2ab(s) {
        const buf = new ArrayBuffer(s.length)
        const view = new Uint8Array(buf)
        for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xff
        return buf
    }
    return blob
}
